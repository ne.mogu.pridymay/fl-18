// 1
function getMaxEvenElement(arr) {
	const evenIdentifer = 2;
	return Math.max.apply(null, arr.filter(item => item%evenIdentifer === 0));
}

// 2
let a = 3; 
let b = 5;

[a, b] = [b, a];

// 3
function getValue(info) {
	return (null ?? info) || (undefined ?? info) ? info: '-';
}

// 4
function arrayOfArrays(arr) {
	return Object.fromEntries(arr);
}

// 5
function addUniqueId(obj) {
	const id = Symbol();
	return {...obj, id};
}

// 6
function getRegroupObject(obj) {
	return {
		university: obj.details.university,
		user: {
			age: obj.details.age,
			firstname: obj.name,
			id: obj.details.id
		}
	}
}

// 7
function getArrayWithUniqueElements(arr) {
	let unique = new Set();
	arr.forEach(item => unique.add(item));
	return Array.from(unique);
}

// 8
function hideNumber(phoneNumber) {
	const 	hideNumbersLength = 6,
			phoneNumberLength = phoneNumber.length;
	return phoneNumber	.slice(hideNumbersLength)
						.padStart(phoneNumberLength, '*');
}

// 9
function add(a, b) {
	if(a === undefined){
		throw'Error: a is required';
	} else if(b === undefined){
		throw'Error: b id required';
	} else{
		return a + b;
	}
}

// 10
function* generateIterableSequence() {
	const identifer = 2;
	let arr = ['I', 'love', 'EPAM'];
	let i = 0;
	while(i <= identifer){
		yield arr[i];
		i++
	}
}
