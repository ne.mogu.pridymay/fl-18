const connection = new WebSocket('ws://localhost:8080');

let userName = '';

connection.onopen = () => {
    userName = prompt('Введіть імя')
};

connection.onclose = () => {
    alert('disconnected');
};

connection.onerror = event => {
    alert(event.type);
};

function createMessage (htmlElement, name, message, time) {
    document.querySelector('.chat').append(htmlElement);
    const divName = document.createElement('div');
    const divMessage = document.createElement('div');
    const divTime = document.createElement('div');
    divName.innerText = name;
    divMessage.innerText = message;
    divTime.innerText = time;
    divName.classList.add('name')
    divMessage.classList.add('message')
    divTime.classList.add('time')
    htmlElement.append(divName, divMessage, divTime);
}


connection.onmessage = event => {
    let data = JSON.parse(event.data)
    const div = document.createElement('div');
    div.classList.add('left-message')
    createMessage(div, data.name, data.message, data.date)
};

document.querySelector('form').addEventListener('submit', event => {
    event.preventDefault();
    const message = document.querySelector('input').value;
    const date = new Date().toLocaleTimeString('ua-UA')
    const data = {
        message,
        name: userName,
        date
    }

    connection.send(JSON.stringify(data));
    document.querySelector('input').value='';

    let rightMessage=document.createElement('div');
    rightMessage.classList.add('right-message')

    createMessage(rightMessage, userName, message, date)
});