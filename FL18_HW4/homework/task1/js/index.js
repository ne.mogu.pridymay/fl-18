window.onload = showSpinner();
hideSpinner();

function showSpinner(){
document.body.className = '';
}

function hideSpinner(){
	document.body.className = 'loaded';
}

let usersList = document.getElementsByClassName('users-list')[0];
let usernames = document.getElementsByClassName('username');
let names = document.getElementsByClassName('name');
let ids = document.getElementsByClassName('id');
let emails = document.getElementsByClassName('email');
let phones = document.getElementsByClassName('phone');
let websites = document.getElementsByClassName('website');
let editButtons = document.getElementsByClassName('edit');
let deleteButtons = document.getElementsByClassName('delete');
let submitButton = document.querySelector('.submit-button');
let editFields = document.getElementsByClassName('field');



let classes = ['username', 'name', 'id', 'email', 'phone', 'website'];
let params = [usernames, names, ids, emails, phones, websites];

const url = 'https://jsonplaceholder.typicode.com/users';

async function loadUsers() {
	try{
		const response = await fetch(url);
		const data = await response.json();
		let clasIndex = 0;
		for(let userIndex = 0; userIndex < data.length; userIndex++){
			clasIndex = 0;
			for(let param of params) {
				param[userIndex].innerHTML = data[userIndex][classes[clasIndex]];
				clasIndex++
			}
		}
	} catch(err){
			console.error(err);
	} 
}

loadUsers();

let container;

for(let button of editButtons){
	button.onclick = function(event){
		let editMenu = document.querySelector('.bg');
		editMenu.style.display = 'block';
		container = event.target.parentElement;
		return container;
	}
}

for(let button of deleteButtons){
	button.onclick = async function(event){
		showSpinner();
		container = event.target.parentElement;
		const deleteUser = {
			method: 'DELETE'
		};
		const deleteResponse = await fetch(`https://jsonplaceholder.typicode.com/users/${container.id}`, deleteUser);
		hideSpinner();

		console.log(deleteResponse.status);
	}
}

submitButton.onclick = async function(){
	document.querySelector('.bg').style.display = 'none';
	showSpinner();
	const newData = {
		username: document.querySelector('.username-field').value,
		name: document.querySelector('.name-field').value,
		id: document.querySelector('.id-field').value,
		email: document.querySelector('.email-field').value,
		phone: document.querySelector('.phone-field').value,
		website: document.querySelector('.website-field').value
	};

	const uploadData = {
		method: 'PUT',
		headers:{
			'Content-type': 'application/json; charset=UTF-8'
		},
		body: JSON.stringify(newData)
	}

	const response = await fetch(`https://jsonplaceholder.typicode.com/users/${container.id}`, uploadData);
	console.log(response.status);
	hideSpinner();
}

