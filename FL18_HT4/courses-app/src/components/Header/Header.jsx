import React, {Fragment} from 'react';
import { useNavigate } from 'react-router';

import {Logo} from './components/Logo/Logo';
import {Button} from '../common/Button/Button';
import {useDispatch} from "react-redux";
import {fetchLogout} from "../../store/user/thunk";

export const Header = () => {
    const dispatch = useDispatch();

    const navigate = useNavigate();

    const user = JSON.parse(localStorage.getItem('user'));

    const handleClick = () => {
        dispatch(fetchLogout(navigate));
    }

    return (<header className='header-container'>
        <Logo/>
        <div className='log-block'>
            {
                    localStorage.getItem('user') !== null ?
                    <Fragment>
                        <span className='log-name'>{user.name}</span>
                        <Button onClick={handleClick} innerText={'Logout'}/>
                    </Fragment> : ''
            }
        </div>
    </header>)
};