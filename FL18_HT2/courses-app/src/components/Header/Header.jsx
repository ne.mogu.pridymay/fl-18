import React, {Fragment} from 'react';
import { useNavigate } from 'react-router';

import {Logo} from './components/Logo/Logo';
import {Button} from '../common/Button/Button';

export const Header = () => {
    const navigate = useNavigate();

    const handleClick = () => {
        localStorage.removeItem('user');
        navigate('/login');
    }

    // localStorage.removeItem('user');    

    return (<header className='header-container'>
        <Logo/>
        <div className='log-block'>
            {
                    localStorage.getItem('user') !== null ?
                    <Fragment>
                        <span className='log-name'>{JSON.parse(localStorage.getItem('user')).user.name}</span>
                        <Button onClick={handleClick} innerText={'Logout'}/>
                    </Fragment> : ''
            }
        </div>
    </header>)
};