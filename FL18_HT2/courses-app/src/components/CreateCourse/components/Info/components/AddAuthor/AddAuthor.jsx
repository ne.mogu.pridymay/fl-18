import React, {useState} from 'react';

import {Input} from "../../../../../common/Input/Input";
import {Button} from "../../../../../common/Button/Button";
import {getAuthor, handlePress} from "../../../../../functions";


export const AddAuthor = ({authorsList, onAddAuthor}) => {
    const [author, setCurrentAuthor] = useState({
        id: '',
        name: ''
    });

    const handleClick = () => {
        return !getAuthor(author.id, authorsList) ?
            onAddAuthor([...authorsList, author]) : undefined
    };

    const onEnterDown = (e) => {
        handlePress(e.keyCode, handleClick);
    };


    return (
        <div className={'author-section form-section'}>
            <h3 className={'author-section-header'}>Add author</h3>
            <Input className={'author-input search-bar'}
                   placeholdetText={'Enter author name...'}
                   onKeyPress={onEnterDown}
                   onChange={(event) => setCurrentAuthor({
                       id: authorsList[authorsList.length - 1].id + '1',
                       name: event.target.value
                   })}
                   labelText={'Author name'}
            />
            <Button secondClass={'create-author-button form-button'} onClick={handleClick}
                    innerText={'Create author'}
            />
        </div>
    )
};