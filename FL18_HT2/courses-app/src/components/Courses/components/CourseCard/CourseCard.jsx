import React, {useState} from 'react';

import {CourseCardDescription} from "../CourseCardDescription/CourseCardDescription";
import {CourseCardInfo} from "../CourseCardInfo/CourseCardInfo";
import {Button} from "../../../common/Button/Button";

import {useNavigate} from "react-router";


export const CourseCard = ({titles, infos, authors, description, title, courseId}) => {
    const [authorsList] = useState(authors);
    const navigate = useNavigate();

    return(
    <article className='course-card-block'>
        <CourseCardDescription title={title} description={description}/>
        <div className='course-card-info-container'>
            <CourseCardInfo titles={titles} infos={infos} authors={authorsList} separator={', '}/>
            <Button onClick={() => navigate(`/courses/${courseId}`)} innerText={'Show course'} secondClass={'show-course-button'}/>
        </div>
    </article>
    )

};