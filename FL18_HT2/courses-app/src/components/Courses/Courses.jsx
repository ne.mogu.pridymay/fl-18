import React, {Fragment, useState} from 'react';

import {CourseCard} from "./components/CourseCard/CourseCard";
import {SearchBar} from "./components/SearchBar/SearchBar";
import {Button} from "../common/Button/Button";


import {getAuthor, getAuthorsList} from "../functions";
import {formattedDate, formattedTime} from "../helpers/pipeDuration";
import {useNavigate} from "react-router";

export const Courses = ({coursesList, authorsList, titles, openForm}) => {
    const [courses, filterCourses] = useState(coursesList);
    const [authors] = useState(authorsList);

    const navigate = useNavigate();

    return (
        <Fragment>
            <div className={'search-block'}>
                <SearchBar onClick={filterCourses} copyOfCourses={coursesList} targetAuthors={authorsList}/>
                <Button secondClass={'add-new-courseCard-button course-button'} onClick={() => navigate('/courses/add')}
                        innerText={'Add new course'}/>
            </div>
            <div>
                {
                    courses.map(course => {
                        const authorsArray = getAuthorsList(course, authors);
                        return (
                            <CourseCard key={course.id} titles={titles} description={course.description}
                                        infos={[authors, formattedTime(course.duration), formattedDate(course.creationDate)]}
                                        title={course.title} authors={authorsArray} courseId={course.id}/>
                        )
                    })
                }
            </div>
        </Fragment>


    );
};
