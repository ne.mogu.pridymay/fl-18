import React, {Fragment, useEffect, useState} from 'react';
import {Link} from "react-router-dom";

import {Input} from "../common/Input/Input";
import {Button} from "../common/Button/Button";
import {useNavigate} from "react-router";

export const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');


    const navigate = useNavigate();

    const user = {
        email: email,
        password: password
    }
    const checkFields = (params, check) => {
        return params.some(param => param === check);

    }
    const handleClick = async () => {
        const params = Object.values(user);
        if (checkFields(params, '')) {
            alert('Заполните все поля');
        } else {
            const result = await fetch('http://localhost:4000/login', {
                method: 'POST',
                body: JSON.stringify(user),
                headers: {
                    'Content-type': 'application/json',
                },
            })
            const data = await result.json();
            if(!data.successful){
                alert('Неверные данные');
                return;
            }
            await localStorage.setItem('user', JSON.stringify(data));
            await navigate('/courses');
        }
    };

    return (
        <Fragment>
            <form className={'login-form main-form'}>
                <h1>Login</h1>
                <Input
                    onChange={event => setEmail(event.target.value)}
                    value={email}
                    className={'search-bar email-input'}
                    onKeyPress={null}
                    labelText={'Email'}
                    placeholdetText={'Enter email'}
                    type={'email'}
                />
                <Input
                    onChange={event => setPassword(event.target.value)}
                    value={password}
                    className={'search-bar password-input'}
                    onKeyPress={null}
                    labelText={'Password'}
                    placeholdetText={'Enter password'}
                    type={'text'}
                />
                <Button
                    secondClass={'log-button login-button'}
                    onClick={handleClick}
                    innerText={'Login'}
                />
                <span className={'registration-link-block'}>If you not have an account you can <Link to="/registration"
                                                                                                     className={'registration-link'}>Registration</Link></span>
            </form>
        </Fragment>

    );
};