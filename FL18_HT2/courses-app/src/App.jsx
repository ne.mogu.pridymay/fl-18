import React, {Fragment, useEffect, useState} from 'react';
import {Routes, Route, Link} from "react-router-dom";

import {mockedCoursesList, mockedAuthorsList} from "./components/constants";

import {Header} from './components/Header/Header';
import {Courses} from "./components/Courses/Courses";
import {CreateCourse} from "./components/CreateCourse/CreateCourse";
import {Registration} from "./components/Registration/Registration";
import {Login} from "./components/Login/Login";
import {CourseInfo} from "./components/CourseInfo/CourseInfo";
import {useNavigate} from "react-router";

const titles = ['Authors', 'Duration', 'Crated'];


export const App = () => {
    const [courses, addCourse] = useState(mockedCoursesList);
    const [authors, addAuthor] = useState(mockedAuthorsList);
    const navigate = useNavigate();

    useEffect(() => {
        navigate(localStorage.getItem('user') ? '/courses': '/login');
    }, []);



    return (
        <Fragment>
            <Header/>
            <main className={'main-section'}>
            {/*     Content     */}
            </main>
            <Routes>
                <Route path={'/registration'} element={<Registration changeStatus={null}/>}/>
                <Route path={'/login'} element={<Login changeStatus={null}/>}/>
                <Route path={'/courses'} element={
                    <Courses
                        titles={titles}
                        coursesList={courses}
                        authorsList={authors}
                        openForm={null}/>}
                />
                <Route path={'/courses/add'} element={
                    <CreateCourse
                        coursesList={courses}
                        onAddCourse={addCourse}
                        authorsList={authors}
                        onAddAuthor={addAuthor}
                        navigate={navigate}/>
                }
                />
                <Route path={'/courses/:courseId'} element={<CourseInfo courses={courses} authors={authors}/>}/>
            </Routes>

        </Fragment>
    );
};