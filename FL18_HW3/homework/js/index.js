'use strict';

// *
//  * Class
//  * @constructor
//  * @param size - size of pizza
//  * @param type - type of pizza
//  * @throws {PizzaException} - in case of improper use
const ArgumentsSize = 1;
const pizzaArgumentsSize = 2;
const one = 1;
 class Meal{
    constructor(options){
        this.size = options.size;
        this.type = options.type;
        this.extraIngredients = options.extraIngredients;
    }
    addExtraIngredient(ingredient){
        if(arguments.length !== ArgumentsSize){
            throw new PizzaException('Reguires one argument');
        } else if(!Pizza.allowedExtraIngredients.includes(ingredient)){
            throw new PizzaException('We have no such ingredients');
        } else if(this.extraIngredients.includes(ingredient)){
            throw new PizzaException('Duplicate ingredient');
        } else{
            this.extraIngredients.push(ingredient);
        }
    }
    removeExtraIngredient(ingredient){
        if(arguments.length !== ArgumentsSize){
            throw new PizzaException('Reguires one argument');
        } else if(!Pizza.allowedExtraIngredients.includes(ingredient)){
            throw new PizzaException('We have no such ingredients');
        } else if(!this.extraIngredients.includes(ingredient)){
            throw new PizzaException('No such ingredient in your pizza');
        }
        this.extraIngredients.splice(this.extraIngredients.indexOf(ingredient), one);
    }
    getPrice(){
        let price = 0;
        for(let param in this){
          if(!Array.isArray(this[param])){
            Object.entries(this[param]).map(function([key]) {
              price += +key;
              return price;
            })
          } else{
              this[param].map(function(ingredient){
                  return Object.entries(ingredient).map(function([key]) {
                    price += +key;
                    return price;
                  })
              });
            }
        }
        return price;
    }
    getSize(){
        return +Object.keys(this.size)[0];
    }
    getExtraIngredients(){
        return this.extraIngredients;
    }
    getPizzaInfo(){
        let info = '';
        for(let param in this){
            if(!Array.isArray(this[param])){
                Object.entries(this[param]).map(function([, value]) {
                  info += `${param}:${value.toUpperCase()}, `;
                  return info;
                });
            } else{
                info += 'extra ingredients: ';
                this[param].map(function(ingredient){
                    return Object.entries(ingredient).map(function([, value]) {
                      info += `${value.toUpperCase()}, `;
                      return info;
                    });
                });
            }
        }
        return info + ` price:${this.getPrice()} UAH`;
    }
 }
function Pizza(size, type) {
    if(arguments.length !== pizzaArgumentsSize){
        throw new PizzaException('Reguired two arguments, given 1');
    } else if(!Pizza.allowedSizes.includes(size) || !Pizza.allowedTypes.includes(type)){
        throw new PizzaException('Invalid type');
    } else{
        return new Meal({
            size: size,
            type: type,
            extraIngredients: []
        });  
    }
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = {50:'small'};
Pizza.SIZE_M = {75:'middle'};
Pizza.SIZE_L = {100:'large'};

Pizza.TYPE_VEGGIE = {50:'veggie'};
Pizza.TYPE_MARGHERITA = {60:'margherita'};
Pizza.TYPE_PEPPERONI = {70:'pepperoni'};

Pizza.EXTRA_TOMATOES = {5:'tomatoes'};
Pizza.EXTRA_CHEESE = {7:'cheese'};
Pizza.EXTRA_MEAT = {9:'meat'};

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];


// *
//  * Provides information about an error while working with a pizza.
//  * details are stored in the log property.
//  * @constructor
 
function PizzaException(exception) {
    this.log = exception;
}

// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient
