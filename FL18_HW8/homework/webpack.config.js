const path = require('path');
const miniCss = require('mini-css-extract-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
   entry: './js/index.js',
   output: {
      filename: './js/app.js',
      path: path.resolve(__dirname, 'dist')
   },
   module: {
      rules: [{
         test: /\.m?js$/,
         use: {  
            loader: ['babel-loader', 'eslint-loader'],
            options: {presets: ['@babel/preset-env']}
         },
         exclude: /(node_modules|bower_components)/,
         test: /\.s(a|c)ss$/,
         use: [
            //   'style-loader',
            miniCss.loader,
            'css-loader',  
            'sass-loader',
         ],
      }],
   },
   plugins: [
      new miniCss({
         filename: './css/style.css',
      }),
      new HtmlWebpackPlugin({
         template: path.resolve(__dirname, './index.html'),
         filename: 'index.html'
      })
   ]
};
