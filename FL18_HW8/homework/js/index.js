import {Part, scissorIndex, paperIndex, rockIndex, scissor, paper, rock} from './objects.js'

import * as Elements from './elem.js'

import '../scss/styles.scss';

const requiredIds = ['Scissor', 'Paper', 'Rock'];
const randomIndex = Math.floor(Math.random() * (requiredIds.length));
const buttons = new Map([
	['Scissor', scissor],
	['Paper', paper],
	['Rock', rock]
]);
function showResult() {
	Elements.$resultField.style.visibility = 'visible';			
	setTimeout(function() {
		Elements.$resultField.style.visibility = 'hidden';
	}, 1500);
}

const buttonsNames = Object.values(buttons.keys());
let wins = 0;
let loses = 0;
let games = 1;
for(let $button of Object.values(Elements)){
	if(requiredIds.includes($button.id)){
		$button.onclick = (event) => {
			if(wins < 3 && loses < 3){
				if(buttons.get(event.target.id).isWin(buttons.get(requiredIds[randomIndex])) === true){
					Elements.$resultField.innerHTML = `<span>Round ${games}, ${event.target.id} vs. ${requiredIds[randomIndex]}, You’ve <span id="result" class="win">WON</span>!</span>`;
					games++;
					wins++;
				} else if(buttons.get(event.target.id).isWin(buttons.get(requiredIds[randomIndex])) === 'draw'){
					Elements.$resultField.innerHTML = `<span>Round ${games}, ${event.target.id} vs. ${requiredIds[randomIndex]}, <span id="result" class="draw">DRAW</span>!</span>`;	
					games++;
				} else{
					Elements.$resultField.innerHTML = `<span>Round ${games}, ${event.target.id} vs. ${requiredIds[randomIndex]}, You’ve <span id="result" class="lose">LOST</span>!</span>`;
					games++;
					loses++;
				}
			}
			showResult();

			if(loses === 3 || wins === 3){
				setTimeout(function() {
					if(loses === 3){
						Elements.$resultField.innerHTML = '<span>Oh no, you\'ve <span id="result" class="lose">LOST</span>!</span>';
					} else if(wins === 3){
						Elements.$resultField.innerHTML = '<span>Congratilations, you\'ve <span id="result" class="win">WON</span>!</span>';
					}
					games = 1;
					wins = loses = 0;
					showResult();
				}, 1500);
			}
		}
	} else if($button.id === 'reset'){
		$button.onclick = function(event) {
			games = 1;
			wins = 0;
			loses = 0;
		}
	}
}