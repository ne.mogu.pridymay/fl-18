export const $scissorButton = document.getElementById('Scissor');
export const $paperButton = document.getElementById('Paper');
export const $rockButton = document.getElementById('Rock');
export const $resetButton = document.getElementById('reset');
export const $resultField = document.querySelector('.bg-result');
export const $result = document.getElementById('result');