export class Part{
	constructor(name, beats){
		this.name = name;
		this.beats = beats;
	}

	isWin(opponent){
		if(opponent.name === this.beats && opponent.name !== this.name) return true;
		else if(opponent.name === this.name) return 'draw';
		else return false;
	}
}


export const scissorIndex = 1;
export const paperIndex = 2;
export const rockIndex = 3;

export const scissor = new Part(scissorIndex, paperIndex);

export const paper = new Part(paperIndex, rockIndex);

export const rock = new Part(rockIndex, scissorIndex);