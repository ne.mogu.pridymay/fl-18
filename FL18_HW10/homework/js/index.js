import {t, cells, scoreBlock} from './elements.js';
import {move, playerScore, AIscore} from './functions.js';
import '../scss/style.scss';

for(let cell of cells){
  cell.onclick = function () {move(cell.id, 'player')};
}

scoreBlock.innerHTML = `You: ${playerScore}\nAI: ${AIscore}`;