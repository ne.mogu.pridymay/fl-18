import {t, scoreBlock, cells, newGameButton, clearButton} from './elements.js';

export let playerScore = 0;
export let AIscore = 0;

function ai() {
  var id = Math.floor(Math.random() * 9);
  t[id] ? ai() : move(id, 'ai');
}

export function move(id, role = 'player') {
  debugger
    if(t[id]) return false;
    t[id] = role;
    document.getElementById(id).classList.add(role);
    setTimeout(function() {
      debugger
      if(checkEnd() !== 'draw' && !checkEnd()){
        if(role === 'player'){
          ai();
        }
      } else if(checkEnd === 'draw'){
        AIszore++;
        playerScore++;
        reset('draw');
      } else{
        if(role === 'player'){
          playerScore++;
        } else{
          AIszore++;
        }
        reset(role);
      }
    }, 500);
}

function setScore() {
  scoreBlock.innerHTML = `You: ${playerScore}\nAI: ${AIscore}`;
}

function gridClear() {
  for(let cell of cells){
    cell.classList.remove('player', 'ai');
  }
}
newGameButton.onclick = function (event) {
  gridClear();
  playerScore, AIscore = 0;
}

clearButton.onclick = function (event) {
  gridClear();

}




function reset(role) {
  alert("Игра окончена!");
  if(role === 'ai'){
    alert('AI wins!');
    setScore();
  } else if(role === 'player'){
    alert('You win!');
    setScore();
  } else{
    alert('DRAW');
  }
  for(let cell of cells){
    cell.classList.remove('player', 'ai');
  }
  t.length = 0;
  t.length = 9;
}



function checkEnd() {
  if (t[0]=='ai' && t[1]=='ai' && t[2]=='ai' || t[0]=='player' && t[1]=='player' && t[2]=='player')  return true;
  if (t[3]=='ai' && t[4]=='ai' && t[5]=='ai' || t[3]=='player' && t[4]=='player' && t[5]=='player')  return true;
  if (t[6]=='ai' && t[7]=='ai' && t[8]=='ai' || t[6]=='player' && t[7]=='player' && t[8]=='player')  return true;
  if (t[0]=='ai' && t[3]=='ai' && t[6]=='ai' || t[0]=='player' && t[3]=='player' && t[6]=='player')  return true;
  if (t[1]=='ai' && t[4]=='ai' && t[7]=='ai' || t[1]=='player' && t[4]=='player' && t[7]=='player')  return true;
  if (t[2]=='ai' && t[5]=='ai' && t[8]=='ai' || t[2]=='player' && t[5]=='player' && t[8]=='player')  return true;
  if (t[0]=='ai' && t[4]=='ai' && t[8]=='ai' || t[0]=='player' && t[4]=='player' && t[8]=='player')  return true;
  if (t[2]=='ai' && t[4]=='ai' && t[6]=='ai' || t[2]=='player' && t[4]=='player' && t[6]=='player')  return true;
  if(t[0] && t[1] && t[2] && t[3] && t[4] && t[5] && t[6] && t[7] && t[8]) return 'draw';
}