 export interface UserInterface {
  readonly id: number
  readonly firstName: string
  readonly lastName: string
  login: string
  email: string
  password: number
  isAuthenticated: boolean
}
