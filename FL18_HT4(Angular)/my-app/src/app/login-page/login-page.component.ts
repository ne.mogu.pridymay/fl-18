  import {Component, Input, Output, EventEmitter} from '@angular/core';
  import {AuthenticationService} from "../../services/authentication-service/authentication.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {
  @Input()
  isLoggedIn: boolean = false;

  @Output() loginStatusChangeEvent = new EventEmitter<boolean>();

  email: string = '';
  password: string = '';
  svc: AuthenticationService | undefined;


  constructor(svc: AuthenticationService) {
    this.svc = svc;
  }

  onLoginClick(){
    const userInfo = {
      email: this.email,
      password: this.password,
    }
    const token: string = 'Bearer test';
    this!.svc!.login(userInfo, token);
    this.isLoggedIn = !this.isLoggedIn;
    this.loginStatusChangeEvent.emit(this.isLoggedIn);
    console.log('logged in successfully');
  }

}
