import {Component, EventEmitter, Input} from '@angular/core';
import {Course} from "./interface";
import {GetCoursesService} from "../../services/get-courses-service/get-courses-service.service";


@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css']
})
export class CourseCardComponent {
  @Input()
  courses: Course[] = [];

  isModalActive: boolean = false;
  svc: GetCoursesService | any;

  constructor(svc: GetCoursesService){ 
    this.svc = svc;
  }

  consoleId(id: number){
    console.log(id);
  }

  onDeleteClick(id: number, index: number) {
    const answer = confirm("Are you sure?");
    if(answer){
      this.courses.splice(index, 1);
      this.svc.removeCourse(id);
      console.log(this.svc.getCoures());
      this.isModalActive = !this.isModalActive;
    }
  }
}
