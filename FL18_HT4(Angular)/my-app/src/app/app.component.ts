import {Component, OnInit} from '@angular/core';
import {listOfCourses} from "../mockedData/mockedData";
import {Course} from "./course-card/interface";
import { GetCoursesService } from 'src/services/get-courses-service/get-courses-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  courses: Course[] = [];
  title = 'my-app';
  isLoggedIn: boolean = false;

  changeLoginStatus(newStatus: boolean){
    this.isLoggedIn = newStatus;
  }

  constructor(svc: GetCoursesService){
    this.courses = svc.getCoures();
  }
}
