import { TestBed } from '@angular/core/testing';

import { GetCoursesServiceService } from './get-courses-service.service';

describe('GetCoursesServiceService', () => {
  let service: GetCoursesServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetCoursesServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
