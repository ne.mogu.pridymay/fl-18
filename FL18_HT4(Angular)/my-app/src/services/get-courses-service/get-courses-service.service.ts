import { Injectable } from '@angular/core';
import { Course } from 'src/app/course-card/interface';

@Injectable({
  providedIn: 'root'
})
export class GetCoursesService {
  courses: Course[] = [
    {
      id: 1,
      title: 'Test Title1',
      creationDate: +(new Date(2022, 7)),
      duration: 120,
      description: 'Test Description1',
      topRated: true
    },
    {
      id: 2,
      title: 'Test Title2',
      creationDate: Date.now(),
      duration: 100,
      description: 'Test Description2',
      topRated: false
    },
    {
      id: 3,
      title: 'Test Title3',
      creationDate: Date.now() + 200000,
      duration: 200,
      description: 'Test Description3',
      topRated: true
    }
  ]


  getCoures() {
    return this.courses;
  }

  addCourse(course: Course) {
    return this.courses.push(course);
  }

  getCourseById(id: number) {
    return this.courses[id - 1];
  }

  updateCourse(course: Course) {
    this.courses[course.id - 1] = course;
  }

  removeCourse(id: number) {
    debugger
    this.courses = this.courses.filter(course => course.id !== id)
  }


  constructor() { }
}
