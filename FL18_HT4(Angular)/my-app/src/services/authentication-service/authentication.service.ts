import { Injectable } from '@angular/core';
import { UserInterface } from 'src/app/userInterface';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  users: UserInterface[] = [
    {
      id: 1,
      firstName: 'Andrey',
      lastName: 'Cherniak',
      login: 'test',
      email: 'test@gmail.com',
      password: 123,
      isAuthenticated: false
    },
  ];



  changeAuthentication(id: number){
    this.users[id - 1].isAuthenticated = !this.users[id - 1].isAuthenticated;
  }

  getUserByEmail(email: string): UserInterface{
    let currentUser: any;
    for(let user of this.users){
      if(user.email === email) {
        currentUser = user;
      }
    }

    return currentUser;
  }

  changeLoginStatus(email: string){
    const user = this.getUserByEmail(email);
    console.log(user);
    const answer = this.changeAuthentication(user!.id);
  }


  login(userInfo: any, token: string){
    debugger
    localStorage.setItem('user', JSON.stringify(userInfo));
    localStorage.setItem('token', token.toString());
    this.changeLoginStatus(userInfo.email);
    console.log(userInfo, token);
  }

  logout(){
    let localUser: string | null = localStorage.getItem('user');
    localUser ??= '';
    const user = JSON.parse(localUser);
    this.changeLoginStatus(user!.email);
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.changeAuthentication(user.id);
  }

  getAuthenticationStatus(email: string){
    let localUser: string | null = localStorage!.getItem('user');
    localUser ??= '';
    const parsedUser = JSON.parse(localUser);
    const user = this.getUserByEmail(parsedUser!.email);

    return user.isAuthenticated;
  }

  getUserInfo(){
    const localUser = localStorage.getItem('user');
    if(!localUser) return;
    const parsedUser = JSON.parse(localUser);
    const user = this.getUserByEmail(parsedUser!.email);

    return user.login;
  }

  constructor() { }
}
