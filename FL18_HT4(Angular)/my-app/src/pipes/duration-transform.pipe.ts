import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'appDurationTransform'
})

export class DurationTransformPipe implements PipeTransform{
  transform(value: number, format: string) {
    const hour: number = 60;

    const ifValueEmpty: any = (value: string, type: string) => {
      return +value > 0 ? value + type: '';
    }
    const getFraction: any = (value: number, devideValue: number) => {
      return (value / devideValue) - Math.trunc(value / devideValue);
    }


    let hours: string = Math.trunc(value / hour).toString();
    let minutes: string = (getFraction(value, hour) * hour).toFixed(0);

    let test = (value % hour);
    debugger
    return format.replace('hh', ifValueEmpty(hours, 'h')).replace('mm', ifValueEmpty(minutes, 'min'));
  }
}
