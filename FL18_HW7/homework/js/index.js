$(document).ready(function () {
	let $logsBlock = $('.logs-block');
	let $equations = document.getElementsByClassName('equation');
	let matchButtons = document.getElementsByClassName('circle');
	let deleteButtons = document.getElementsByClassName('delete');
	let endIndex = -1;

	function calculate(fn) {
		return new Function('return ' + fn)();
	}

	$('.math-button').click(function (event) {
		let operatorCheck = new RegExp(/[*+-/]$/ig);
		let screen = $('.screen');
		let targetClassList = event.target.classList;
		let targetInnerHTML = event.target.innerHTML;
		if(targetClassList.contains('result')){
			if(calculate(screen.text()) === Infinity || isNaN(calculate(screen.text()))){
				screen.text('ERROR');
			} else{
				$logsBlock.prepend(`   
							<li class="log">
					          <div class="circle"></div>
					          <span class="equation">${screen.text()}=${calculate(screen.text())}</span>
					          <div class="delete">
					            <div class="left-line"></div>
					            <div class="right-line"></div>
					          </div>
					      	</li>`)
				for(let matchButton of matchButtons){
					matchButton.onclick = function (event) {
						$(event.target).css('background-color', function(){
							if($(event.target).css('background-color') === 'rgb(255, 0, 0)'){
								return 'transparent';
							} else{
								return 'red';
							}
						});
					};
				}

				for(let deleteButton of deleteButtons){
					deleteButton.onclick = function(event) {
						event.target.parentElement.parentElement.remove();
					}
				}
				for(let equation of $equations){
					if(~$(equation).text().indexOf('48')){
						!$(equation).hasClass('marked-equation') ? 
						$(equation).toggleClass('marked-equation'): undefined;
					} else{
						continue
					}
				}
				screen.text(calculate(screen.text()));

			}
		} else if(operatorCheck.test(screen.text()) && targetClassList.contains('operator')){
			screen.text(screen.text().slice(0, endIndex) + targetInnerHTML);
		} else if(targetClassList.contains('clear')){
			screen.text('');
		} else{
			if(screen.text() === 'ERROR') {
				screen.text(targetInnerHTML); 
			} else {
				screen.text(screen.text() + targetInnerHTML); 
			}
		}
	});

	$('.logs-block').scroll(function() {
		console.log(`Scroll Top: ${$('.logs-block').scrollTop()}`);
	});
});
