import { Component } from '@angular/core';
import {Course} from "./interface";

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css']
})
export class CourseCardComponent {
  course: Course = {
    id: '1',
    title: 'Test Title',
    creationDate: Date.now(),
    duration: '1h 28min',
    description: 'Test Description'
  }
}
