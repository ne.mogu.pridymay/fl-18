export interface Course {
  id: string
  title: string
  creationDate: number
  duration: string
  description: string
}
