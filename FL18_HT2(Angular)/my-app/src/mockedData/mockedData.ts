import {Course} from "../app/course-card/interface";

export const listOfCourses: Course[] = [
  {
    id: 1,
    title: 'Test Title1',
    creationDate: Date.now(),
    duration: '1h 28min',
    description: 'Test Description1'
  },
  {
    id: 2,
    title: 'Test Title2',
    creationDate: Date.now(),
    duration: '3h 50min',
    description: 'Test Description2'
  },
  {
    id: 3,
    title: 'Test Title3',
    creationDate: Date.now(),
    duration: '0h 30min',
    description: 'Test Description3'
  }
]
