export interface Course {
  id: number
  title: string
  creationDate: number
  duration: string
  description: string
}
