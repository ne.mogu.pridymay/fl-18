import React, {Fragment, useState} from 'react';

import {mockedCoursesList, mockedAuthorsList} from "./components/constants";

import {Header} from './components/Header/Header';
import {Courses} from "./components/Courses/Courses";
import {CreateCourse} from "./components/CreateCourse/CreateCourse";

const titles = ['Authors' ,'Duration', 'Crated'];


export const App = () => {
    const [RenderComponent, setRenderComponent] = useState('Courses');
    const [courses, addCourse] = useState(mockedCoursesList);
    const [authors, addAuthor] = useState(mockedAuthorsList);

    return (
        <Fragment>
            <Header name={'Andrey'}/>
            <main className={'main-section'}>
                {
                    RenderComponent === 'Courses'
                        ? <Courses
                            titles={titles}
                            coursesList={courses}
                            authorsList={authors}
                            openForm={setRenderComponent}/>
                        : <CreateCourse
                            coursesList={courses}
                            onAddCourse={addCourse}
                            authorsList={authors}
                            onAddAuthor={addAuthor}
                            openCourses={setRenderComponent}/>

                }
            </main>

        </Fragment>
    );
};