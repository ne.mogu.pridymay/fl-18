import React, {useEffect, useState} from 'react';

import {Title} from "./components/Title/Title";
import {Button} from "../common/Button/Button";
import {CourseDescription} from "./components/Description/CourseDescription";
import {Info} from "./components/Info/Info";
import {checkFields, createCourseAndOpenCourses} from "../functions";

export const CreateCourse = (props) => {
    const [newCourse, addCourse] = useState({
        id: '',
        title: '',// string
        description: '',// string
        creationDate: '',// string
        duration: 0,// number
        authors: [],// [authorId]
    });
    const fieldsForCheck = {
        title: '',// string
        description: '',// string
        creationDate: '',// string
        duration: 0,// number
        authors: [],// [authorId]
    }

    useEffect(() => {
        addCourse({
            ...newCourse,
            id: Date.now().toString(),
            creationDate: new Date().toLocaleDateString('ua-UA', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
            })
        })
    }, [newCourse]);

    return (
        <section className={'create-course-block'}>
            <div className={'title-description-block'}>
                <div className={'title-block'}>
                    <Title onAddTitle={addCourse} course={newCourse} required/>
                    <Button secondClass={'crate-course-button'}
                            innerText={'Create course'}
                            onClick={() => checkFields(newCourse, fieldsForCheck) ?
                                createCourseAndOpenCourses(props, newCourse, addCourse) :
                                alert('Заполните все поля')
                            }
                    />
                </div>
                <CourseDescription className={'crate-course-description'}
                                   labelText={'Description'}
                                   placeholdetText={'Enter description'}
                                   onAddDescription={addCourse}
                                   course={newCourse}
                />
            </div>
            <Info course={newCourse} onAddInfo={addCourse} {...props}/>
        </section>
    );
}