import React, {Fragment, useState} from 'react';

import {Button} from "../../../../../common/Button/Button";
import {CourseAuthors} from "../CourseAuthors/CourseAuthors";
import {getIDArr, makeArrayOfINames} from "../../../../../functions";

export const AuthorsList = ({authorsList, onAddAuthor, addInfo, course}) => {
    const [innerText] = useState('Add author');

    return (
        <Fragment>
            <div className={'authors-list-container form-section'}>
                <h3 className={'authors-list-header'}>Authors</h3>
                <ul className={'authors-list'}>
                    {
                        authorsList.map(author => {
                            return (
                                <li className={'add-author-li'} key={author.id}>
                                    <span className={'author'}>{author.name}</span>
                                    <Button onClick={() => addInfo({
                                        ...course, authors: getIDArr(course.authors, author.id)
                                    })}
                                            innerText={innerText}
                                            secondClass={'add-author-button form-button'}
                                            key={author.id}
                                    />
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
            <CourseAuthors authorsList={makeArrayOfINames(authorsList, course.authors)}/>
        </Fragment>
    )
};