import React, {useState} from 'react';


import {Input} from "../../../common/Input/Input";
import {Button} from "../../../common/Button/Button";
import {filterCourses, handlePress} from "../../../functions";

export const SearchBar = ({onClick, targetAuthors, copyOfCourses}) => {
    const [value, setCurrentValue] = useState('');

    const handleFilter = () => {
        filterCourses(onClick, copyOfCourses, value, targetAuthors)
    };

    const onEnterDown = (e) => {
        handlePress(e.keyCode, handleFilter);
    };

    return (
      <div className={'search-bar-block'}>
          <Input onKeyPress={onEnterDown} className={'search-bar'} onChange={event => setCurrentValue(event.target.value.toLowerCase())} placeholdetText={'Enter course name...'}/>
          <Button secondClass={'search-button course-button'} innerText={'Search'} onClick={handleFilter} onKey/>
      </div>
    );
};
