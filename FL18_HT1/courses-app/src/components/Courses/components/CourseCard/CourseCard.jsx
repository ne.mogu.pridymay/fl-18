import React, {useState} from 'react';

import {CourseCardDescription} from "../CourseCardDescription/CourseCardDescription";
import {CourseCardInfo} from "../CourseCardInfo/CourseCardInfo";
import {Button} from "../../../common/Button/Button";


export const CourseCard = ({titles, infos, authors, description, title}) => {
    const [authorsList] = useState(authors);

    return(
    <article className='course-card-block'>
        <CourseCardDescription title={title} description={description}/>
        <div className='course-card-info-container'>
            <CourseCardInfo titles={titles} infos={infos} authors={authorsList}/>
            <Button onClick={null} innerText={'Show course'} secondClass={'show-course-button'}/>
        </div>
    </article>
    )

};