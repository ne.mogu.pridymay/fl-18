import React from 'react';

export const CourseCardInfoListLi = ({title, info}) => (
    <li className='course-card-info-punkt'>
        <span className='punkt-title'>{title}: </span>
        <span className={'punkt-info'}>{info}</span>
    </li>
);