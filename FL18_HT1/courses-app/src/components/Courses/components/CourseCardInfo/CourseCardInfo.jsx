import React from 'react';

import  {CourseCardInfoListLi} from "./CourseCardInfoListLi";
export const CourseCardInfo = ({titles, infos, authors}) => (
    <div className='course-card-info'>
        <ul className='course-card-info-list'>
            {
                titles.map((title, index) => (
                <CourseCardInfoListLi key={index} title={title} info={title === 'Authors' ? authors.join(', '): infos[index]}/>
                ))
            }
        </ul>
    </div>
);