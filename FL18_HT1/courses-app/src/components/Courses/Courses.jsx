import React, {Fragment, useState} from 'react';

import {CourseCard} from "./components/CourseCard/CourseCard";
import {SearchBar} from "./components/SearchBar/SearchBar";
import {Button} from "../common/Button/Button";


import {getAuthor} from "../functions";
import {formattedDate, formattedTime} from "../helpers/pipeDuration";




export const Courses = ({coursesList, authorsList, titles, openForm}) => {
    const [courses, filterCourses] = useState(coursesList);
    const [authors] = useState(authorsList);

    let authorsArray = [];


    return (
        <Fragment>
            <div className={'search-block'}>
                <SearchBar onClick={filterCourses} copyOfCourses={coursesList} targetAuthors={authorsList}/>
                <Button secondClass={'add-new-courseCard-button course-button'} onClick={() => openForm('Form')}
                        innerText={'Add new course'}/>
            </div>
            <div>
                {
                    courses.map(course => {
                        authorsArray = [];
                        for (let authorID of course.authors) {
                            authorsArray.push(getAuthor(authorID, authors));
                        }
                        return (
                            <CourseCard key={course.id} titles={titles} description={course.description}
                                        infos={[authors, formattedTime(course.duration), formattedDate(course.creationDate)]}
                                        title={course.title} authors={authorsArray}/>
                        )
                    })
                }
            </div>
        </Fragment>


    );
};
