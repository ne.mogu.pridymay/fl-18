import React from 'react';

import {Logo} from './components/Logo/Logo';
import {Button} from '../common/Button/Button';

export const Header = (props) => (
    <header className='header-container'>
        <Logo/>
        <div className='log-block'>
            <span className='log-name'>{props.name}</span>
            <Button onClick={null} innerText={'Logout'}/>
        </div>
    </header>
);