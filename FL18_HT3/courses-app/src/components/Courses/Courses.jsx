import React, {Fragment} from 'react';

import {CourseCard} from "./components/CourseCard/CourseCard";
import {SearchBar} from "./components/SearchBar/SearchBar";
import {Button} from "../common/Button/Button";


import {formattedDate, formattedTime} from "../helpers/pipeDuration";
import {useNavigate} from "react-router";
import {useDispatch, useSelector} from "react-redux";

export const Courses = ({coursesList, titles}) => {
    const authors = useSelector(state => state.authors);
    const courses = useSelector(state => state.courses);
    const dispatch = useDispatch();

    const navigate = useNavigate();

    return (
        <Fragment>
            <div className={'search-block'}>
                <SearchBar onClick={dispatch} copyOfCourses={coursesList} targetAuthors={authors}/>
                <Button secondClass={'add-new-courseCard-button course-button'} onClick={() => navigate('/courses/add')}
                        innerText={'Add new course'}/>
            </div>
            <div>
                {
                    courses.map(course => {
                        return (
                            <CourseCard key={course.id} titles={titles} description={course.description} course={course}
                                        infos={[authors, formattedTime(course.duration), formattedDate(course.creationDate)]}
                                        title={course.title} courseId={course.id}/>
                        )
                    })
                }
            </div>
        </Fragment>


    );
};
