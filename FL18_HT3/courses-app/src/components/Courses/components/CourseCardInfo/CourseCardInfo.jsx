import React from 'react';

import  {CourseCardInfoListLi} from "./CourseCardInfoListLi";
import {useSelector} from "react-redux";
import {getAuthorsList} from "../../../functions";
export const CourseCardInfo = ({titles, infos, separator, course}) => {
    debugger
    const authors = useSelector(state => state.authors);
    const authorsArray = getAuthorsList(course, authors);
    return (<div className='course-card-info'>
        <ul className='course-card-info-list'>
            {
                titles.map((title, index) => (
                    <CourseCardInfoListLi
                        key={index}
                        title={title}
                        info={title === 'Authors' ? authorsArray.join(separator) : infos[index]}
                    />
                ))
            }
        </ul>
    </div>)
};