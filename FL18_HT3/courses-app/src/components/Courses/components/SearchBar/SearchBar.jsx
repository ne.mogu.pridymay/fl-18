import React, {useState} from 'react';


import {Input} from "../../../common/Input/Input";
import {Button} from "../../../common/Button/Button";
import {filterCourses, handlePress} from "../../../functions";
import {useDispatch, useSelector} from "react-redux";
import {createCourseAction} from "../../../../store/courses/actionCreators";
import {SORT} from "../../../../store/courses/actionTypes";

export const SearchBar = () => {
    const [value, setCurrentValue] = useState('');

    const addCourseAct = createCourseAction;
    const sort = SORT;
    const authors = useSelector(state => state.authors);
    const dispatch = useDispatch();

    const handleFilter = () => {
        filterCourses(dispatch, value, authors, addCourseAct, sort);
    };

    const onEnterDown = (e) => {
        handlePress(e.keyCode, handleFilter);
    };

    return (
      <div className={'search-bar-block'}>
          <Input onKeyPress={onEnterDown} className={'search-bar'} onChange={event => setCurrentValue(event.target.value.toLowerCase())} placeholdetText={'Enter course name...'}/>
          <Button secondClass={'search-button course-button'} innerText={'Search'} onClick={handleFilter} onKey/>
      </div>
    );
};
