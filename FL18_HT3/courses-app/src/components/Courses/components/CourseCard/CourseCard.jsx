import React from 'react';

import {CourseCardDescription} from "../CourseCardDescription/CourseCardDescription";
import {CourseCardInfo} from "../CourseCardInfo/CourseCardInfo";
import {Button} from "../../../common/Button/Button";

import {useNavigate} from "react-router";
import {useDispatch} from "react-redux";
import {DELETE_COURSE} from "../../../../store/courses/actionTypes";
import {createCourseAction} from "../../../../store/courses/actionCreators";


export const CourseCard = ({titles, infos, description, title, courseId, ...props}) => {
    const deleteCourse = DELETE_COURSE;
    const delCourseAct = createCourseAction;
    const dispatch = useDispatch();

    const navigate = useNavigate();

    const deleteCourseFun = () => {
        dispatch(delCourseAct(deleteCourse, courseId));
    };

    return(
    <article className='course-card-block'>
        <CourseCardDescription title={title} description={description}/>
        <div className='course-card-info-container'>
            <CourseCardInfo titles={titles} infos={infos} separator={', '} {...props}/>
            <div className={'course-operations-block'}>
                <Button onClick={() => navigate(`/courses/${courseId}`)} innerText={'Show course'} secondClass={'show-course-button'}/>
                <Button onClick={null} secondClass={'edit-button course-operation'} innerText={<img src={require('../../../../assets/pencil-fill.svg').default}
                                                                                   alt="Edit"/>}/>
                <Button onClick={deleteCourseFun} secondClass={'delete-button course-operation'} innerText={<img src={require('../../../../assets/delete-bin-6-line.svg').default}
                                                                                                    alt="Delete"/>}/>
            </div>
        </div>
    </article>
    )

};