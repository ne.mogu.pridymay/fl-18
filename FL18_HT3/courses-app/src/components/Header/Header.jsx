import React, {Fragment} from 'react';
import { useNavigate } from 'react-router';

import {Logo} from './components/Logo/Logo';
import {Button} from '../common/Button/Button';
import {useDispatch} from "react-redux";
import {LOGOUT} from "../../store/user/actionTypes";
import {createLoginAction} from "../../store/user/actionCreators";

export const Header = () => {
    const logout = LOGOUT;
    const logoutAct = createLoginAction;
    const dispath = useDispatch();

    const navigate = useNavigate();

    const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).user : '';

    const handleClick = () => {
        dispath(logoutAct(logout));
        localStorage.removeItem('user');
        debugger
        navigate('/login');
    }

    // localStorage.removeItem('user');    

    return (<header className='header-container'>
        <Logo/>
        <div className='log-block'>
            {
                    localStorage.getItem('user') !== null ?
                    <Fragment>
                        <span className='log-name'>{user.name}</span>
                        <Button onClick={handleClick} innerText={'Logout'}/>
                    </Fragment> : ''
            }
        </div>
    </header>)
};