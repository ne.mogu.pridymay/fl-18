import {checkParamsFields} from "./components/functions";

export const loadCourses = async (dispatch, act, type) => {
    const response = await fetch('http://localhost:4000/courses/all');
    const data = await response.json();
    dispatch(act(type, data.result));
    debugger
};
export const loadAuthors = async (dispatch, act, type) => {
    const response = await fetch('http://localhost:4000/authors/all');
    const data = await response.json();
    dispatch(act(type, data.result));
};

export const clearAll = async (dispatch, act, type) => {
    dispatch(act.userAct(type.clearCourse));
    dispatch(act.authorAct(type.clearAuthor));
};

export const handleRegisterClick = async (user, navigate) => {
    const params = Object.values(user);
    if (checkParamsFields(params, '')) {
        alert('Заполните все поля');
    } else {
        debugger
        const response = await fetch('http://localhost:4000/register', {
            method: 'POST',
            body: JSON.stringify(user),
            headers: {
                'Content-type': 'application/json',
            },
        });
        const data = await response.json();
        if (!data.successful) {
            const errors = data.errors.join('\n');
            alert(errors);
            return;
        }
        navigate('/login');
    }
};

export const handleLoginClick = async (user, navigate, dispath, act, type) => {
    const params = Object.values(user);
    if (checkParamsFields(params, '')) {
        alert('Заполните все поля');
    } else {
        const result = await fetch('http://localhost:4000/login', {
            method: 'POST',
            body: JSON.stringify(user),
            headers: {
                'Content-type': 'application/json',
            },
        })
        const data = await result.json();
        if(!data.successful){
            alert('Неверные данные');
            return;
        }
        dispath(act(type, {
            isAuth: true,
            name: data.user.name,
            email: data.user.email,
            token: JSON.stringify(data),
        }))
        await localStorage.setItem('user', JSON.stringify(data));
        await navigate('/courses');
    }
};