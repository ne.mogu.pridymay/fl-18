const defaultState = [];

export const CourseReducer = (state = defaultState, action) => {
        switch (action.type) {
            case 'LOAD_COURSES':
                let startState = [...state, ...action.payload];
                localStorage.setItem('copyOfCourses', JSON.stringify(startState));
                return startState
            case 'ADD_COURSE':
                return [...state, action.payload]
            case 'DELETE_COURSE':
                let newState = state.filter(course => course.id !== action.payload);
                localStorage.setItem('copyOfCourses', JSON.stringify(newState));
                return newState;
            case 'CLEAR_ALL':
                return defaultState
            case 'SORT':
                return action.payload
            default:
                return state
        }
};