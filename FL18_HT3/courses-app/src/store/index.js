import {createStore} from "redux";

import {composeWithDevTools} from "redux-devtools-extension";
const {combineReducers} = require("redux");
const {UserReducer} = require("./user/reducer");
const {CourseReducer} = require("./courses/reducer");
const {AuthorReducer} = require("./authors/reducer");

const rootReducer = combineReducers({
    user: UserReducer,
    courses: CourseReducer,
    authors: AuthorReducer
})

export const store = createStore(rootReducer, composeWithDevTools());