import React, {Fragment, useEffect} from 'react';
import {Routes, Route} from "react-router-dom";


import {Header} from './components/Header/Header';
import {Courses} from "./components/Courses/Courses";
import {CreateCourse} from "./components/CreateCourse/CreateCourse";
import {Registration} from "./components/Registration/Registration";
import {Login} from "./components/Login/Login";
import {CourseInfo} from "./components/CourseInfo/CourseInfo";
import {useNavigate} from "react-router";

import {useSelector, useDispatch} from "react-redux";
import {createCourseAction} from "./store/courses/actionCreators";
import {CLEAR_ALL_COURSES, LOAD_COURSES} from "./store/courses/actionTypes";
import {createAuthorAction} from "./store/authors/actionCreators";
import {CLEAR_ALL_AUTHORS, LOAD_AUTHORS} from "./store/authors/actionTypes";
import {clearAll, loadAuthors, loadCourses} from "./services";

const titles = ['Authors', 'Duration', 'Crated'];


export const App = () => {
    const addCourseAct = createCourseAction;
    const addAuthorAct = createAuthorAction;
    const loadAllCourses = LOAD_COURSES;
    const loadAllAuthors = LOAD_AUTHORS;
    const clearCourses = CLEAR_ALL_COURSES;
    const clearAuthors = CLEAR_ALL_AUTHORS;

    const authors = useSelector(state => state.authors);
    const dispatch = useDispatch();

    const navigate = useNavigate();




    useEffect(() => {
        loadCourses(dispatch, addCourseAct, loadAllCourses);
        loadAuthors(dispatch, addAuthorAct, loadAllAuthors);
        navigate(localStorage.getItem('user') ? '/courses': '/login');
        return () => {
            clearAll(dispatch, {userAct: addCourseAct, authorAct: addAuthorAct}, {clearCourse: clearCourses, clearAuthor: clearAuthors});
        };
    }, []);


    return (
        <Fragment>
            <Header/>
            <main className={'main-section'}>
                {/*     Content     */}
            </main>
            <Routes>
                <Route path={'/registration'} element={<Registration changeStatus={null}/>}/>
                <Route path={'/login'} element={<Login changeStatus={null}/>}/>
                <Route path={'/courses'} element={
                    <Courses
                        titles={titles}
                        authorsList={authors}
                    />}
                />
                <Route path={'/courses/add'} element={
                    <CreateCourse navigate={navigate}/>
                }
                />
                <Route path={'/courses/:courseId'} element={<CourseInfo/>}/>
            </Routes>

        </Fragment>
    );
};