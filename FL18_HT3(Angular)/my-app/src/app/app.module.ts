import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BreadcrumpsComponent } from './breadcrumps/breadcrumps.component';
import { SectionComponent } from './section/section.component';
import { CourseCardComponent } from './course-card/course-card.component';
import { LoadMoreComponent } from './load-more/load-more.component';
import { FooterComponent } from './footer/footer.component';
import {FormsModule} from "@angular/forms";
import {IfnewDirective} from "../directives/ifnew.directive";
import {DurationTransformPipe} from "../pipes/duration-transform.pipe";
import {orderByPipe} from "../pipes/orderBy.pipe";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BreadcrumpsComponent,
    SectionComponent,
    CourseCardComponent,
    LoadMoreComponent,
    FooterComponent,
    IfnewDirective,
    DurationTransformPipe,
    orderByPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
