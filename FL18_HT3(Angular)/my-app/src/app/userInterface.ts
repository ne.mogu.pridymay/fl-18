 export interface UserInterface {
  readonly id: number
  firstName: string
  lastName: string
}
