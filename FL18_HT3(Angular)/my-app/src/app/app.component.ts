import {Component, OnInit} from '@angular/core';
import {listOfCourses} from "../mockedData/mockedData";
import {Course} from "./course-card/interface";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  courses: Course[] = [];
  title = 'my-app';

  ngOnInit() {
    this.courses = listOfCourses;
  }
}
