import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Course} from "./interface";

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css']
})
export class CourseCardComponent {
  @Input()
  courses: Course[] = [];

  @Output()
  onDelete = new EventEmitter();

  consoleId(id: number){
    console.log(id);
  }

  onDeleteClick(id: number, index: number) {
    this.courses.splice(index, 1);
    this.onDelete.emit();
  }
}
