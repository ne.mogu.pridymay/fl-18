import {Course} from "../app/course-card/interface";

export const listOfCourses: Course[] = [
  {
    id: 1,
    title: 'Test Title1',
    creationDate: +(new Date(2022, 7)),
    duration: 120,
    description: 'Test Description1',
    topRated: true
  },
  {
    id: 2,
    title: 'Test Title2',
    creationDate: Date.now(),
    duration: 100,
    description: 'Test Description2',
    topRated: false
  },
  {
    id: 3,
    title: 'Test Title3',
    creationDate: Date.now() + 200000,
    duration: 200,
    description: 'Test Description3',
    topRated: true
  }
]
