import {Pipe, PipeTransform} from "@angular/core";
import {Course} from "../app/course-card/interface";

@Pipe({
  name: 'appOrderByPipe'
})

export class orderByPipe implements PipeTransform{
  transform(courses: Course[]) {
    return courses.sort((course: any, nextCourse: any) => {
      return nextCourse.creationDate - course.creationDate;
    })
  }
}
