import {Directive, ElementRef, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appIfnew]'
})
export class IfnewDirective {

  @Input() creationDate: number = 0;

  color: string = '';

  constructor(private renderer: Renderer2,
              private el: ElementRef) {
    if (this.creationDate < Date.now() && this.creationDate >= (Date.now() - this.twoWeeks)) {
      this.color = 'green';
    } else if(this.creationDate > Date.now()){
      this.color = 'blue';
    }
    renderer.setStyle(el.nativeElement, 'border', '1px solid' + this.color);

  }


  twoWeeks: number = 14 * 24 * 60 * 60 * 100;
}
